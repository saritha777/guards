import { Body, Controller, Get, Post, Req, Res, SetMetadata, UseGuards } from '@nestjs/common';
import { AppService } from './app.service';
import { UserLogin } from './entity/login.entity';
import { User } from './entity/user.entity';
import { Roles } from './roles.decorator';
import { RolesGuard } from './roles.guard';
import { Request, Response } from 'express';

@Controller()
@UseGuards(RolesGuard)
export class AppController {

  constructor(private readonly appService: AppService) {}

  @Get()
 // @UseGuards(RolesGuard)
 //@SetMetadata('roles', 'admin')
 @Roles('admin')
  getHello(): string {
    return this.appService.getHello();
  }

  @Get('hi')
  //@SetMetadata('roles', 'public')
  //@Roles('public')
  getHi(): string {
    return 'hiiiii';
  }

  @Post()
  @Roles('admin')
  async postUser( @Body() user: User) {
    return this.appService.addUser(user)
  }

  @Post('/login')
  @Roles('user')
  async login(@Body() login: UserLogin,@Res({passthrough:true}) response:Response) {
      return this.appService.userLogin(login,response);
    }

    @Get('/userlogin')
    async user(@Req() request:Request){
      return this.appService.findUser(request);
    }

    @Post('/logout')
    async logout(@Res({passthrough:true}) response:Response){
       return this.appService.logout(response);
    }
}


