import { ApiProperty } from "@nestjs/swagger";
import { IsString } from "class-validator";
import { Column, Entity, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { UserLogin } from "./login.entity";

@Entity()
export class User {

    @PrimaryGeneratedColumn()
    id: number;

    @IsString()
    @ApiProperty()
    @Column()
    name: string;

    @Column({unique: true})
    email: string;

    @IsString()
    @ApiProperty()
    @Column()
    password: string;

    @OneToOne( () => UserLogin, login => login.user, {
        cascade: true,
      })
      login: UserLogin;
}