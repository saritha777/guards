import { BadRequestException, Injectable, Res, UnauthorizedException } from '@nestjs/common';
import { User } from './entity/user.entity';
import * as bcrypt from 'bcryptjs';
import { UserLogin } from './entity/login.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { JwtService } from '@nestjs/jwt';
import { Request, Response} from 'express';

@Injectable()
export class AppService {

  constructor(
    @InjectRepository(User)
  private userRepository: Repository<User>,
  private jwtService: JwtService
  ){}

  getHello(): string {
    return 'Hello World!';
  }

  async addUser(data: User): Promise<User> {
    const pass = await bcrypt.hash(data.password, 10);
    const user = new User();
    const login = new UserLogin()
     user.name= data.name;
     user.email=login.email =data.email;
     user.password = login.password = pass;
     
     user.login = login;
   //  delete user.password;
     return this.userRepository.save(user);
   }

  async userLogin(data: UserLogin, @Res() response:Response) {
    const user = await this.userRepository.findOne({email: data.email});
    console.log(user);
    if(!user) {
      throw new BadRequestException('invalid credentials');
    }
    if(!await bcrypt.compare(data.password,user.password)){
      throw new BadRequestException('pasward incorrect');
    }
    const jwt = await this.jwtService.signAsync({id: user.id})
    response.cookie('jwt',jwt,{httpOnly: true});
    return {
      message: 'success'
    };
  }

  // find user using cookie
  async findUser(request: Request){
    try {
      const cookie= request.cookies['jwt'];

      const data = await this.jwtService.verifyAsync(cookie);
      if(!data) {
        throw new UnauthorizedException();
      }
      const user = await this.userRepository.findOne({id: data['id']});
      const {password, ...result} = user;
      return result;
      } catch(e){
        throw new UnauthorizedException();
      }
  }

  //logout user
  async logout(@Res({passthrough:true}) response:Response){
    response.clearCookie('jwt');
    return {
      message: 'logout'
    }

  }
}
