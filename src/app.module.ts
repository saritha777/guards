import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserLogin } from './entity/login.entity';
import { User } from './entity/user.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: 'root',
      database: 'guard',
      autoLoadEntities: true,
      synchronize: true
    }),
    TypeOrmModule.forFeature([User, UserLogin]),
    JwtModule.register({
      secret: 'secret',
      signOptions: { expiresIn: '30m' }
    })
  ],
  controllers: [AppController],
  providers: [AppService]
})
export class AppModule {}
